﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Repository.Models;

namespace Services
{
    public static class RolesInitialize
    {
        public static void RolesInitializer(RoleManager<IdentityRole> roleManager)
        {
            if (roleManager.FindByNameAsync("sponsor").Result == null)
            {
                roleManager.CreateAsync(new IdentityRole("sponsor")).Wait();
            }
            if (roleManager.FindByNameAsync("manager").Result == null)
            {
                roleManager.CreateAsync(new IdentityRole("manager")).Wait();
            }
            if (roleManager.FindByNameAsync("researcher").Result == null)
            {
                roleManager.CreateAsync(new IdentityRole("researcher")).Wait();
            }
        }
    }
}