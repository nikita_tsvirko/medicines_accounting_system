﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Repository;
using Repository.Models;
using Services.Interfaces;

namespace Services.Realizations
{
    public class PatientsService : IPatientsService
    {
        private readonly DatabaseContext _dbContext;
        private readonly string _connectionString;
        private int pageSize = 3;
        public int TotalPages { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }

        public PatientsService(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
            if (dbContext != null)
                _connectionString = dbContext.Database.GetDbConnection().ConnectionString;
        }
        public bool GetHasPreviousPage()
        {
            return HasPreviousPage;
        }

        public bool GetHasNextPage()
        {
            return HasNextPage;
        }

        public int CountPatients()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.QueryFirstOrDefault<int>("SELECT COUNT(Id) FROM Patients");
                return result;
            }
        }

        public int CountPatientsInClinic(int clinicId)
        {
            var result = GetPatientsFromClinic(clinicId).Count;
            return result;
        }

        public bool AddPatient(Patient patient)
        {
            bool result;
            using (_dbContext)
            {
                var storages = _dbContext.Clinics.Include(c => c.Storages).ToList().FirstOrDefault(x => x.Id == patient.ClinicId)
                    ?.Storages;
                if (storages != null && storages.Count > 0)
                { 
                    Random rand = new Random();
                    var randStorage = storages[rand.Next(storages.Count)];
                    patient.MedicineId = randStorage.MedicineId;
                    patient.NumOfVisits = 0;
                    patient.FirstVisit = DateTime.Now;
                    patient.PatientStatusId = _dbContext.PatientStatuses.FirstOrDefault(s => s.Status == "Unknown").Id;
                    _dbContext.Patients.Add(patient);
                    _dbContext.SaveChanges();
                    result = true;
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        public void UpdatePatientStatus(int id, int? newStatusId)
        {
            using (_dbContext)
            {
                var patient = GetPatientById(id);
                patient.PatientStatusId = newStatusId;
                _dbContext.SaveChanges();
            }
        }

        public void EndPatientParticipation(int id)
        {
            var patient = GetPatientById(id);
            patient.DateOfEndParticipation = DateTime.Now;
            patient.Participation = false;
            _dbContext.SaveChanges();
        }

        public List<PatientStatus> GetPatientStatuses()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.Query<PatientStatus>("SELECT * FROM PatientStatuses").ToList();
                return result;
            }
        }

        public Patient GetPatientById(int? id)
        {
            var result = _dbContext.Patients.FirstOrDefault(x => x.Id == id);
            return result;
        }

        public List<Patient> GetPatientsFromClinic(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.Query<Patient>("SELECT * FROM Patients WHERE ClinicId = @id", new { id }).ToList();
                return result;
            }
        }

        public int GetCountOfNewPatientsInMonth()
        {
            var lastMonth = DateTime.Today;
            lastMonth = lastMonth.AddMonths(-1);
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.QueryFirstOrDefault<int>(
                    "SELECT COUNT(Id) FROM Patients " +
                    "WHERE FirstVisit > @lastMonth", new { lastMonth });
                return result;
            }
        }

        public int GetCountEndedParticipationPatientsInMonth()
        {
            var lastMonth = DateTime.Today;
            lastMonth = lastMonth.AddMonths(-1);
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.QueryFirstOrDefault<int>(
                    "SELECT COUNT(Id) FROM Patients " +
                    "WHERE DateOfEndParticipation > @lastMonth", new { lastMonth });
                return result;
            }
        }

        public void SetPageSize(int newPageSize) => pageSize = newPageSize;

        public void CheckPreviosAndNextPage(int page)
        {
            HasPreviousPage = page > 1;
            HasNextPage = page < TotalPages;
        }

        public List<Patient> TakePatientsFromClinic(int clinicId, PatientsSortState sortState = PatientsSortState.NameAsc, int page = 1)
        {
            TotalPages = (int)Math.Ceiling(CountPatientsInClinic(clinicId) / (double)pageSize);
            CheckPreviosAndNextPage(page);
            var result = GetPatientsFromClinic(clinicId);
            int numOfSkip = (page - 1) * pageSize;
            switch (sortState)
            {
                case PatientsSortState.NameDesc:
                    result = result.OrderByDescending(x => x.FullName).ToList();
                    break;
                case PatientsSortState.BirthdayAsc:
                    result = result.OrderBy(x => x.Birthday).ToList();
                    break;
                case PatientsSortState.BirthdayDesc:
                    result = result.OrderByDescending(x => x.Birthday).ToList();
                    break;
                case PatientsSortState.LastVisitAsc:
                    result = result.OrderBy(x => x.LastVisit).ToList();
                    break;
                case PatientsSortState.LastVisitDesc:
                    result = result.OrderByDescending(x => x.LastVisit).ToList();
                    break;
                default:
                    result = result.OrderBy(x => x.FullName).ToList();
                    break;
            }
            return result.Skip(numOfSkip).Take(pageSize).ToList();
        }

        public void InitializeStatuses()
        {
            if (((RelationalDatabaseCreator)_dbContext.GetService<IDatabaseCreator>()).Exists())
                StatusesOfPatientsInitialize.StatusesOfPatientsInitializer(_dbContext);
        }
    }
}
