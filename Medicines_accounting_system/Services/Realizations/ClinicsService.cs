﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Repository;
using Repository.Models;
using Services.Interfaces;

namespace Services.Realizations
{
    public class ClinicsService : IClinicsService
    {
        private readonly DatabaseContext _dbContext;
        private readonly string _connectionString;

        public ClinicsService(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
            if (dbContext != null)
                _connectionString = dbContext.Database.GetDbConnection().ConnectionString;
        }

        public void AddClinic(Clinic clinic)
        {
            using (_dbContext)
            {
                clinic.NumOfVisits = 0;
                _dbContext.Clinics.Add(clinic);
                _dbContext.SaveChanges();
            }
        }

        public List<Clinic> GetClinics()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.Query<Clinic>("SELECT * FROM Clinics").ToList();
                return result;
            }
        }

        public Clinic GetClinicById(int? id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.QueryFirstOrDefault<Clinic>("SELECT * FROM Clinics WHERE Id = @id", new { id });
                return result;
            }
        }

        public Clinic GetClinicByName(string clinicName)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.QueryFirstOrDefault<Clinic>("SELECT * FROM Clinics WHERE Name = @clinicName", new { clinicName });
                return result;
            }
        }

        public Medicine GetMedicineById(int? id)
        {
            var result = _dbContext.Medicines.FirstOrDefault(x => x.Id == id);
            return result;
        }

        public Medicine GetMedicineByMedicineId(string medicineId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.QueryFirstOrDefault<Medicine>("SELECT * FROM Medicines WHERE MedicineId = @medicineId", new { medicineId });
                return result;
            }
        }

        public List<Medicine> GetIssuedMedicinesFromClinic(int? clinicId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.Query<Medicine>(
                    "SELECT * FROM Medicines " +
                    "WHERE Medicines.Id IN " +
                    "(SELECT MedicineId FROM Patients WHERE ClinicId = @clinicId)", 
                    new { clinicId }).ToList();
                return result;
            }
        }

        public int GetCountIssuedMedicinesFromClinic(int? clinicId)
        {
            var result = GetIssuedMedicinesFromClinic(clinicId).Count;
            return result;
        }

        public List<Medicine> GetIssuedMedicinesFromClinicInMonth(int? clinicId)
        {
            var lastMonth = DateTime.Today;
            lastMonth = lastMonth.AddMonths(-1);
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.Query<Medicine>(
                    "SELECT * FROM Medicines " +
                    "WHERE Medicines.Id IN " +
                    "(SELECT MedicineId FROM Patients WHERE ClinicId = @clinicId AND LastVisit > @lastMonth)",
                    new { clinicId, lastMonth }).ToList();
                return result;
            }
        }

        public int GetCountIssuedMedicinesFromClinicInMonth(int? clinicId)
        {
            var result = GetIssuedMedicinesFromClinicInMonth(clinicId).Count;
            return result;
        }

        public List<Storage> GetStoragesFromClinic(int? clinicId)
        {
            var result = _dbContext.Clinics.Include(c => c.Storages).ToList().FirstOrDefault(x => x.Id == clinicId)
                    ?.Storages;
            return result;
        }

        public void AddMedicineToClinic(int clinicId, int medicineId, int count)
        {
            var storages = GetStoragesFromClinic(clinicId);
            if (storages != null)
            {
                var storage = storages.FirstOrDefault(x => x.MedicineId == medicineId && x.ClinicId == clinicId);
                if (storage != null)
                {
                    storage.Count += count;
                }
                else
                {
                    var newStorage = new Storage { MedicineId = medicineId, ClinicId = clinicId, Count = count };
                    _dbContext.Storages.Add(newStorage);

                }
            }
            _dbContext.SaveChanges();
        }
    }
}
