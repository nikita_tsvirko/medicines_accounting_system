﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Repository;
using Repository.Models;
using Services.Interfaces;

namespace Services.Realizations
{
    public class VisitsService : IVisitsService
    {
        public IPatientsService PatientsService;
        private readonly DatabaseContext _dbContext;
        public int NumOfVisitsForPatients { get; set; }
        private readonly string _connectionString;

        public VisitsService(DatabaseContext dbContext, IPatientsService patientsService)
        {
            _dbContext = dbContext;
            PatientsService = patientsService;
            NumOfVisitsForPatients = 10;
            if (dbContext != null)
                _connectionString = dbContext.Database.GetDbConnection().ConnectionString;
        }

        public int CountVisits()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.QueryFirstOrDefault<int>("SELECT COUNT(Id) FROM Visits");
                return result;
            }
        }

        public int GetNumOfVisitsForPatients()
        {
            return NumOfVisitsForPatients;
        }

        public List<Visit> GetVisitsByPatient(int patientId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.Query<Visit>("SELECT * FROM Visits WHERE PatientId = @patientId", 
                    new { patientId }).ToList();
                return result;
            }
        }

        public bool AddVisitByPatient(int patientId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var patient = PatientsService.GetPatientById(patientId);
                var storage = _dbContext.Storages.Include(x => x.Medicine)
                    .FirstOrDefault(s => s.ClinicId == patient.ClinicId && s.MedicineId == patient.MedicineId);
                if (storage == null)
                {
                    return false;
                }

                if (storage.Count == 1)
                {
                    _dbContext.Entry(storage).State = EntityState.Deleted;
                }
                else
                {
                    storage.Count--;
                }
                var medicineId = storage.MedicineId;
                patient.NumOfVisits++;
                var visit = new Visit
                {
                    DateOfVisit = DateTime.Now,
                    MedicineId = medicineId,
                    PatientId = patientId,
                    Num = patient.NumOfVisits
                };
                if (patient.NumOfVisits >= NumOfVisitsForPatients)
                {
                    patient.Participation = false;
                    patient.DateOfEndParticipation = DateTime.Now;
                    patient.PatientStatusId = db.QueryFirstOrDefault<PatientStatus>("SELECT * FROM PatientStatuses WHERE Status = 'Completed the research'").Id;
                }
                patient.LastVisit = visit.DateOfVisit;
                _dbContext.Visits.Add(visit);
                _dbContext.SaveChanges();
                return true;
            }
        }
    }
}
