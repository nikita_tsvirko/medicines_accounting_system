﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Repository;
using Repository.Models;
using Services.Interfaces;

namespace Services.Realizations
{
    public class UsersService : IUsersService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly DatabaseContext _dbContext;
        private int PasswordChangePeriodInDays = 180;
        private int pageSize = 3;
        public int TotalPages { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }

        public UsersService(UserManager<User> userManager, SignInManager<User> signInManager, 
            RoleManager<IdentityRole> roleManager, DatabaseContext dbContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _dbContext = dbContext;
        }

        public async Task Login(User user)
        {
            await _signInManager.SignInAsync(user, false);
        }

        public async Task<string> LoginWithPassword(string login, string password, bool rememberMe)
        {
            var result = await _signInManager.PasswordSignInAsync(login, password, rememberMe, true);
            if (result.IsLockedOut)
            {
                var user = await GetUserByEmailAsync(login);
                return user.BlockingReason ??
                       (user.BlockingReason = "The password was entered too many times incorrectly.");
            }
            if (result.Succeeded || result.RequiresTwoFactor)
            {
                return result.ToString();
            }
            return null;
        }

        public async Task<SignInResult> LoginWithTwoFactor(string code, bool rememberMe, bool rememberClient, string providerName)
        {
            var result = await _signInManager.TwoFactorSignInAsync(providerName, code, rememberMe, rememberClient);
            return result;
        }

        public async Task<SignInResult> LoginWithRecoveryCode(string recoveryCode)
        {
            var result = await _signInManager.TwoFactorRecoveryCodeSignInAsync(recoveryCode);
            return result;
        }

        public async Task Logout()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<IdentityResult> Register(User user, string password)
        {
            var result = await _userManager.CreateAsync(user, password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "researcher");
                user.DatePasswordSet = DateTime.Now;
                await _userManager.UpdateAsync(user);
                await Login(user);
            }
            return result;
        }

        public async Task<IdentityResult> BlockUser(string userId, string blockingReason)
        {
            var user = await GetUserByIdAsync(userId);
            user.Blocked = true;
            user.BlockingReason = blockingReason;
            await _userManager.UpdateAsync(user);
            return await _userManager.SetLockoutEndDateAsync(user, DateTimeOffset.MaxValue);
        }

        public async Task<IdentityResult> UnblockUser(string userId)
        {
            var user = await GetUserByIdAsync(userId);
            user.Blocked = false;
            user.BlockingReason = null;
            await _userManager.UpdateAsync(user);
            return await _userManager.SetLockoutEndDateAsync(user, null);
        }

        public async Task<IdentityResult> UpdateCurrentUser(ClaimsPrincipal userOld, User userUpdate)
        {
            var user = await GetUserAsync(userOld);
            var userRoles = await _userManager.GetRolesAsync(user);
            return await UpdateUserById(user.Id, userUpdate, userRoles.ToList());
        }

        public async Task<IdentityResult> UpdateUserById(string userId, User userUpdate, List<string> rolesUpdate)
        {
            var user = await GetUserByIdAsync(userId);
            user.Blocked = userUpdate.Blocked;
            user.FirstName = userUpdate.FirstName;
            user.LastName = userUpdate.LastName;
            user.Initials = userUpdate.Initials;
            user.Email = userUpdate.Email;
            user.BlockingReason = userUpdate.BlockingReason;
            user.ClinicId = userUpdate.ClinicId;
            user.DatePasswordSet = userUpdate.DatePasswordSet;
            var result = await _userManager.UpdateAsync(user);

            var userRoles = await _userManager.GetRolesAsync(user);
            var addedRoles = rolesUpdate.Except(userRoles);
            var removedRoles = userRoles.Except(rolesUpdate);
            await _userManager.AddToRolesAsync(user, addedRoles);
            await _userManager.RemoveFromRolesAsync(user, removedRoles);

            return result;
        }

        public async Task<IdentityResult> ChangePassword(ClaimsPrincipal userOld, string oldPassword, string newPassword)
        {
            var user = await GetUserAsync(userOld);
            var result = await _userManager.ChangePasswordAsync(user, oldPassword, newPassword);
            if (result.Succeeded)
            {
                user.DatePasswordSet = DateTime.Now;
                await _userManager.UpdateAsync(user);
                await Login(user);
            }
            return result;
        }

        public bool IsSignedIn(ClaimsPrincipal user)
        {
            return _signInManager.IsSignedIn(user);
        }

        public int GetPasswordChangePeriodInDays()
        {
            return PasswordChangePeriodInDays;
        }

        public string GetUserName(ClaimsPrincipal user)
        {
            return _userManager.GetUserName(user);
        }

        public async Task<User> GetUserAsync(ClaimsPrincipal user)
        {
            var result = await _userManager.GetUserAsync(user);
            if (result == null)
            {
                throw new ApplicationException("Unable to load current user.");
            }

            return result;
        }

        public async Task<User> GetUserByIdAsync(string userId)
        {
            var result = await _userManager.FindByIdAsync(userId);
            if (result == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }

            return result;
        }

        public async Task<User> GetUserByUsername(string username)
        {
            var result = await _userManager.FindByNameAsync(username);
            return result;
        }

        public async Task<User> GetUserByEmailAsync(string userEmail)
        {
            var result = await _userManager.FindByEmailAsync(userEmail);
            if (result == null)
            {
                throw new ApplicationException($"Unable to load user with email '{userEmail}'.");
            }

            return result;
        }

        public async Task<bool> UserHasPasswordAsync(User user)
        {
            var result = await _userManager.HasPasswordAsync(user);
            return result;
        }

        public List<User> GetUsers()
        {
            return _userManager.Users.ToList();
        }

        public async Task<List<string>> GetUserRoles(User user)
        {
            var result = await _userManager.GetRolesAsync(user);
            return result.ToList();
        }

        public List<IdentityRole> GetRoles()
        {
            return _roleManager.Roles.ToList();
        }

        public async Task<User> GetTwoFactorAuthenticationUserAsync()
        {
            var result = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (result == null)
            {
                throw new ApplicationException("Unable to load two-factor authentication user.");
            }
            return result;
        }

        public async Task<string> GetAuthenticatorKeyAsync(User user)
        {
            var result = await _userManager.GetAuthenticatorKeyAsync(user);
            return result;
        }

        public async Task<int> CountRecoveryCodesAsync(User user)
        {
            var result = await _userManager.CountRecoveryCodesAsync(user);
            return result;
        }

        public async Task SetTwoFactorEnabledAsync(User user, bool enabled, string providerName)
        {
            var result = await _userManager.SetTwoFactorEnabledAsync(user, enabled);
            user.TwoFactorProviderName = providerName;
            await _userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                throw new ApplicationException($"Unexpected error occured disabling 2FA for user with ID '{user.Id}'.");
            }
        }

        public async Task<bool> VerifyTwoFactorTokenAsync(User user, string code)
        {
            var tokenProvider = _userManager.Options.Tokens.AuthenticatorTokenProvider;
            var result = await _userManager.VerifyTwoFactorTokenAsync(user, tokenProvider, code);
            return result;
        }

        public async Task<string[]> GenerateNewTwoFactorRecoveryCodesAsync(User user)
        {
            var result = (await _userManager.GenerateNewTwoFactorRecoveryCodesAsync(user, 10)).ToArray();
            return result;
        }

        public async Task ResetAuthenticatorKeyAsync(User user)
        {
            await _userManager.ResetAuthenticatorKeyAsync(user);
        }

        public async Task<string> GetTwoFactorTokenAsync(User user)
        {
            var result = await _userManager.GenerateTwoFactorTokenAsync(user, "EmailTwoFactor");
            return result;
        }

        public async Task<string> GetPasswordResetTokenAsync(User user)
        {
            await _userManager.RemovePasswordAsync(user);
            var result = await _userManager.GeneratePasswordResetTokenAsync(user);
            return result;
        }

        public async Task<IdentityResult> ResetPasswordAsync(User user, string code, string newPassword)
        {
            user.DatePasswordSet = DateTime.Now;
            await _userManager.UpdateAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, code, newPassword);
            return result;
        }

        public async Task<string> GetEmailConfirmationTokenAsync(User user)
        {
            var result = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            return result;
        }

        public async Task<bool> IsEmailConfirmedAsync(User user)
        {
            var result = await _userManager.IsEmailConfirmedAsync(user);
            return result;
        }

        public async Task<IdentityResult> ConfirmEmailAsync(User user, string code)
        {
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return result;
        }

        public void SetPageSize(int newPageSize) => pageSize = newPageSize;

        public bool GetHasPreviousPage()
        {
            return HasPreviousPage;
        }

        public bool GetHasNextPage()
        {
            return HasNextPage;
        }

        public void CheckPreviosAndNextPage(int page)
        {
            HasPreviousPage = page > 1;
            HasNextPage = page < TotalPages;
        }

        public int CountUsers()
        {
            var result = _userManager.Users.Count();
            return result;
        }

        public List<User> TakeUsers(int page = 1)
        {
            TotalPages = (int)Math.Ceiling(CountUsers() / (double)pageSize);
            CheckPreviosAndNextPage(page);
            var result = _userManager.Users.ToList();
            int numOfSkip = (page - 1) * pageSize;
            return result.Skip(numOfSkip).Take(pageSize).ToList();
        }

        public void InitializeRoles()
        {
            if(((RelationalDatabaseCreator) _dbContext.GetService<IDatabaseCreator>()).Exists())
                RolesInitialize.RolesInitializer(_roleManager);
        }
    }
}
