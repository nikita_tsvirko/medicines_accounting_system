﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Repository;
using Repository.Models;
using Services.Interfaces;

namespace Services.Realizations
{
    public class StockService : IStockService
    {
        public IClinicsService ClinicsService;
        public IPatientsService PatientsService;
        public IVisitsService VisitsService;
        private readonly DatabaseContext _dbContext;
        private readonly string _connectionString;
        private int pageSize = 3;
        private int CountOfReserveMedicines = 5;
        public int TotalPages { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }

        public StockService(DatabaseContext dbContext, IClinicsService clinicsService,
            IPatientsService patientsService, IVisitsService visitsService)
        {
            _dbContext = dbContext;
            ClinicsService = clinicsService;
            PatientsService = patientsService;
            VisitsService = visitsService;
            if (dbContext != null)
                _connectionString = dbContext.Database.GetDbConnection().ConnectionString;
        }

        public void SetPageSize(int newPageSize) => pageSize = newPageSize;

        public void AddMedicine(Medicine medicine)
        {
            using (_dbContext)
            {
                medicine.AvailabilityOnStock = true;
                medicine.DateOfReceiptToStock = DateTime.Now;
                _dbContext.Medicines.Add(medicine);
                _dbContext.SaveChanges();
            }
        }

        public void DeleteMedicineFromStock(int id)
        {
            using (_dbContext)
            {
                var medicine = _dbContext.Medicines.FirstOrDefault(m => m.Id == id);
                if (medicine != null) medicine.AvailabilityOnStock = false;
                _dbContext.SaveChanges();
            }
        }

        public void UpdateMedicineOnStock(int id, Medicine updateMedicine)
        {
            using (_dbContext)
            {
                var medicine = GetMedicineFromStock(id);
                medicine.NumOnStock = updateMedicine.NumOnStock;
                medicine.Description = updateMedicine.Description;
                medicine.Form = updateMedicine.Form;
                medicine.Container = updateMedicine.Container;
                medicine.Status = updateMedicine.Status;
                medicine.ExpirationDateOfMedicine = updateMedicine.ExpirationDateOfMedicine;
                medicine.AvailabilityOnStock = true;
                _dbContext.SaveChanges();
            }
        }

        public Medicine GetMedicineFromStock(int id)
        {
            var result = _dbContext.Medicines.FirstOrDefault(x => x.AvailabilityOnStock && x.Id == id);
            return result;
        }

        public List<Medicine> GetMedicinesFromStock()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.Query<Medicine>("SELECT * FROM Medicines WHERE AvailabilityOnStock = 'TRUE'").ToList();
                return result;
            }
        }

        public List<Medicine> GetReceivedOnStockMedicinesInMonth()
        {
            var lastMonth = DateTime.Today;
            lastMonth = lastMonth.AddMonths(-1);
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.Query<Medicine>(
                    "SELECT * FROM Medicines " +
                    "WHERE AvailabilityOnStock = 'TRUE' AND DateOfReceiptToStock > @lastMonth",
                    new {  lastMonth }).ToList();
                return result;
            }
        }

        public bool SendMedicine(int medicineId, int count)
        {
            var medicine = GetMedicineFromStock(medicineId);
            if (medicine != null && medicine.NumOnStock >= count)
            {
                medicine.NumOnStock -= count;
                _dbContext.SaveChanges();
                return true;
            }

            return false;
        }

        public bool SendMedicinesFromExcelTable(IFormFile file, string newPath)
        {
            if (file.Length > 0)
            {
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                string fullPath = Path.Combine(newPath, file.FileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;
                    var hssfwb = sFileExtension == ".xls" ? (IWorkbook) new HSSFWorkbook(stream) : new XSSFWorkbook(stream);
                    ISheet sheet = hssfwb.GetSheetAt(0);
                    for (int i = sheet.FirstRowNum; i <= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        if (row == null) continue;
                        if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                        var medicineID = row.GetCell(row.FirstCellNum).ToString();
                        var countStr = row.GetCell(row.FirstCellNum + 1).ToString();
                        var clinicName = row.GetCell(row.FirstCellNum + 2).ToString();
                        if (clinicName != null && int.TryParse(countStr, out var count) && count > 0)
                        {
                            var clinic = ClinicsService.GetClinicByName(clinicName);
                            var medicine = ClinicsService.GetMedicineByMedicineId(medicineID);
                            if (clinic != null && medicine != null && SendMedicine(medicine.Id, count))
                            {
                                ClinicsService.AddMedicineToClinic(clinic.Id, medicine.Id, count);
                            }
                            else
                                return false;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            return false;
        }

        public List<Storage> AutomaticSendMedicinesToClinic()
        {
            var order = new List<Storage>();
            var clinics = ClinicsService.GetClinics();
            foreach (var clinic in clinics)
            {
                var patients = PatientsService.GetPatientsFromClinic(clinic.Id);
                foreach (var patient in patients)
                {
                    if (patient.Participation)
                    {
                        var medicine = ClinicsService.GetMedicineById(patient.MedicineId);
                        var count = VisitsService.GetNumOfVisitsForPatients() - patient.NumOfVisits;
                        var storage = order.FirstOrDefault(s => s.MedicineId == medicine.Id && s.ClinicId == clinic.Id);
                        if (storage == null)
                        {
                            var newStorage = new Storage
                            {
                                Medicine = medicine,
                                MedicineId = medicine.Id,
                                ClinicId = clinic.Id,
                                Count = count + CountOfReserveMedicines
                            };
                            order.Add(newStorage);
                        }
                        else
                        {
                            storage.Count += count;
                        }
                    }
                }
            }

            foreach (var storage in order)
            {
                var medicine = storage.Medicine;
                var updateStorage =
                    _dbContext.Storages.FirstOrDefault(s => s.MedicineId == storage.MedicineId && s.ClinicId == storage.ClinicId);
                if (updateStorage == null)
                {
                    if (medicine.NumOnStock < storage.Count)
                    {
                        return null;
                    }
                    _dbContext.Storages.Add(storage);
                }
                else
                {
                    storage.Count -= updateStorage.Count;
                    if (storage.Count <= 0)
                    {
                        storage.Count = 0;
                        continue;
                    }
                    if (medicine.NumOnStock < storage.Count)
                    {
                        return null;
                    }
                    updateStorage.Count += storage.Count;
                }

                medicine.NumOnStock -= storage.Count;
            }

            _dbContext.SaveChanges();
            return order;
        }

        public void CheckPreviosAndNextPage(int page)
        {
            HasPreviousPage = page > 1;
            HasNextPage = page < TotalPages;
        }

        public int CountMedicinesOnStock()
        {
            var result = GetMedicinesFromStock().Count;
            return result;
        }

        public List<Medicine> TakeMedicinesFromStock(StockSortState sortState = StockSortState.IdAsc, int page = 1)
        {
            TotalPages = (int)Math.Ceiling(CountMedicinesOnStock() / (double)pageSize);
            CheckPreviosAndNextPage(page);
            var result = _dbContext.Medicines.Where(x => x.AvailabilityOnStock).ToList();
            int numOfSkip = (page - 1) * pageSize;
            switch (sortState)
            {
                case StockSortState.IdDesc:
                    result = result.OrderByDescending(x => x.MedicineId).ToList();
                    break;
                case StockSortState.FormAsc:
                    result = result.OrderBy(x => x.Form).ToList();
                    break;
                case StockSortState.FormDesc:
                    result = result.OrderByDescending(x => x.Form).ToList();
                    break;
                case StockSortState.ContainerAsc:
                    result = result.OrderBy(x => x.Container).ToList();
                    break;
                case StockSortState.ContainerDesc:
                    result = result.OrderByDescending(x => x.Container).ToList();
                    break;
                case StockSortState.NumOfStockAsc:
                    result = result.OrderBy(x => x.NumOnStock).ToList();
                    break;
                case StockSortState.NumOfStockDesc:
                    result = result.OrderByDescending(x => x.NumOnStock).ToList();
                    break;
                case StockSortState.ExpirationDateOfMedicineAsc:
                    result = result.OrderBy(x => x.ExpirationDateOfMedicine).ToList();
                    break;
                case StockSortState.ExpirationDateOfMedicineDesc:
                    result = result.OrderByDescending(x => x.ExpirationDateOfMedicine).ToList();
                    break;
                default:
                    result = result.OrderBy(x => x.MedicineId).ToList();
                    break;
            }
            return result.Skip(numOfSkip).Take(pageSize).ToList();
        }

        public async Task<MemoryStream> ExportMedicinesToExcel(string path, string fileName)
        {
            var memory = new MemoryStream();
            using (var fs = new FileStream(Path.Combine(path, fileName), FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Stock");
                IRow row = excelSheet.CreateRow(0);

                row.CreateCell(0).SetCellValue("ID");
                row.CreateCell(1).SetCellValue("Description");
                row.CreateCell(2).SetCellValue("Form");
                row.CreateCell(3).SetCellValue("Container");
                row.CreateCell(4).SetCellValue("Number");
                row.CreateCell(5).SetCellValue("Status");
                row.CreateCell(6).SetCellValue("Expiration date");

                var medicines = GetMedicinesFromStock();
                int i = 1;
                foreach (var medicine in medicines)
                {
                    row = excelSheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(medicine.MedicineId);
                    row.CreateCell(1).SetCellValue(medicine.Description);
                    row.CreateCell(2).SetCellValue(medicine.Form);
                    row.CreateCell(3).SetCellValue(medicine.Container);
                    row.CreateCell(4).SetCellValue(medicine.NumOnStock);
                    row.CreateCell(5).SetCellValue(medicine.Status);
                    var cell = row.CreateCell(6);
                    var cellStyle = workbook.CreateCellStyle();
                    var format = workbook.CreateDataFormat();
                    cellStyle.DataFormat = format.GetFormat("dd.mm.yyyy;@");
                    cell.CellStyle = cellStyle;
                    cell.SetCellValue(medicine.ExpirationDateOfMedicine);
                    i++;
                }

                workbook.Write(fs);
            }
            using (var stream = new FileStream(Path.Combine(path, fileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return memory;
        }
    }
}
