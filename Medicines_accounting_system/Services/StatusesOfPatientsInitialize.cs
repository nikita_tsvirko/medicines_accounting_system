﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Repository;
using Repository.Models;

namespace Services
{
    public static class StatusesOfPatientsInitialize
    {
        public static void StatusesOfPatientsInitializer(DatabaseContext dbContext)
        {
            if (dbContext.PatientStatuses.FirstOrDefaultAsync(s => s.Status == "Unknown").Result == null)
            {
                dbContext.PatientStatuses.Add(new PatientStatus { Status = "Unknown"});
            }
            if (dbContext.PatientStatuses.FirstOrDefaultAsync(s => s.Status == "Passed screening").Result == null)
            {
                dbContext.PatientStatuses.Add(new PatientStatus { Status = "Passed screening" });
            }
            if (dbContext.PatientStatuses.FirstOrDefaultAsync(s => s.Status == "Randomized").Result == null)
            {
                dbContext.PatientStatuses.Add(new PatientStatus { Status = "Randomized" });
            }
            if (dbContext.PatientStatuses.FirstOrDefaultAsync(s => s.Status == "Early completed the research").Result == null)
            {
                dbContext.PatientStatuses.Add(new PatientStatus { Status = "Early completed the research" });
            }
            if (dbContext.PatientStatuses.FirstOrDefaultAsync(s => s.Status == "Completed the research").Result == null)
            {
                dbContext.PatientStatuses.Add(new PatientStatus { Status = "Completed the research" });
            }

            dbContext.SaveChanges();
        }
    }
}
