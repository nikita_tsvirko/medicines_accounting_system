﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public enum PatientsSortState
    {
        NameAsc,
        NameDesc,
        BirthdayAsc,
        BirthdayDesc,
        LastVisitAsc,
        LastVisitDesc
    }
}