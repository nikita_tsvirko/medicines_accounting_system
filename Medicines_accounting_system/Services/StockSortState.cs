﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public enum StockSortState
    {
        IdAsc,
        IdDesc,
        FormAsc,
        FormDesc,
        ContainerAsc,
        ContainerDesc,
        NumOfStockAsc,
        NumOfStockDesc,
        ExpirationDateOfMedicineAsc,
        ExpirationDateOfMedicineDesc
    }
}
