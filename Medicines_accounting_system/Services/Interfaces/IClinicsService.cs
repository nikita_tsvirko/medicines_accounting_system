﻿using System;
using System.Collections.Generic;
using System.Text;
using Repository.Models;

namespace Services.Interfaces
{
    public interface IClinicsService
    {
        void AddClinic(Clinic clinic);
        List<Clinic> GetClinics();
        Clinic GetClinicById(int? id);
        Clinic GetClinicByName(string clinicName);
        Medicine GetMedicineById(int? id);
        Medicine GetMedicineByMedicineId(string medicineId);
        List<Medicine> GetIssuedMedicinesFromClinic(int? clinicId);
        int GetCountIssuedMedicinesFromClinic(int? clinicId);
        List<Medicine> GetIssuedMedicinesFromClinicInMonth(int? clinicId);
        int GetCountIssuedMedicinesFromClinicInMonth(int? clinicId);
        List<Storage> GetStoragesFromClinic(int? clinicId);
        void AddMedicineToClinic(int clinicId, int medicineId, int count);
    }
}
