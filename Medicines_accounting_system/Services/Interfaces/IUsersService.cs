﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Repository.Models;

namespace Services.Interfaces
{
    public interface IUsersService
    {
        Task Login(User user);
        Task<string> LoginWithPassword(string login, string password, bool rememberMe);
        Task<SignInResult> LoginWithTwoFactor(string code, bool rememberMe, bool rememberClient,
            string providerName);
        Task<SignInResult> LoginWithRecoveryCode(string recoveryCode);
        Task Logout();
        Task<IdentityResult> Register(User user, string password);
        Task<IdentityResult> BlockUser(string userId, string blockingReason);
        Task<IdentityResult> UnblockUser(string userId);
        Task<IdentityResult> UpdateCurrentUser(ClaimsPrincipal userOld, User userUpdate);
        Task<IdentityResult> UpdateUserById(string userId, User userUpdate, List<string> rolesUpdate);
        Task<IdentityResult> ChangePassword(ClaimsPrincipal userOld, string oldPassword, string newPassword);
        bool IsSignedIn(ClaimsPrincipal user);
        int GetPasswordChangePeriodInDays();
        string GetUserName(ClaimsPrincipal user);
        Task<User> GetUserAsync(ClaimsPrincipal user);
        Task<User> GetUserByIdAsync(string userId);
        Task<User> GetUserByEmailAsync(string userEmail);
        Task<User> GetUserByUsername(string username);
        Task<bool> UserHasPasswordAsync(User user);
        List<User> GetUsers();
        Task<List<string>> GetUserRoles(User user);
        List<IdentityRole> GetRoles();
        Task<User> GetTwoFactorAuthenticationUserAsync();
        Task<string> GetAuthenticatorKeyAsync(User user);
        Task<int> CountRecoveryCodesAsync(User user);
        Task SetTwoFactorEnabledAsync(User user, bool enabled, string providerName);
        Task<bool> VerifyTwoFactorTokenAsync(User user, string code);
        Task<string[]> GenerateNewTwoFactorRecoveryCodesAsync(User user);
        Task ResetAuthenticatorKeyAsync(User user);
        Task<string> GetTwoFactorTokenAsync(User user);
        Task<string> GetPasswordResetTokenAsync(User user);
        Task<IdentityResult> ResetPasswordAsync(User user, string code, string newPassword);
        Task<string> GetEmailConfirmationTokenAsync(User user);
        Task<bool> IsEmailConfirmedAsync(User user);
        Task<IdentityResult> ConfirmEmailAsync(User user, string code);
        void SetPageSize(int newPageSize);
        bool GetHasPreviousPage();
        bool GetHasNextPage();
        void CheckPreviosAndNextPage(int page);
        int CountUsers();
        List<User> TakeUsers(int page = 1);
        void InitializeRoles();
    }
}