﻿using System;
using System.Collections.Generic;
using System.Text;
using Repository.Models;

namespace Services.Interfaces
{
    public interface IVisitsService
    {
        int CountVisits();
        int GetNumOfVisitsForPatients();
        List<Visit> GetVisitsByPatient(int patientId);
        bool AddVisitByPatient(int patientId);
    }
}
