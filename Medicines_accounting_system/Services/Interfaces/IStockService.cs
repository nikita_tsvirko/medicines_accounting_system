﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Repository.Models;

namespace Services.Interfaces
{
    public interface IStockService
    {
        void SetPageSize(int newPageSize);
        void AddMedicine(Medicine medicine);
        void UpdateMedicineOnStock(int id, Medicine updateMedicine);
        Medicine GetMedicineFromStock(int id);
        List<Medicine> GetMedicinesFromStock();
        List<Medicine> GetReceivedOnStockMedicinesInMonth();
        void DeleteMedicineFromStock(int id);
        bool SendMedicine(int medicineId, int count);
        bool SendMedicinesFromExcelTable(IFormFile file, string newPath);
        List<Storage> AutomaticSendMedicinesToClinic();
        void CheckPreviosAndNextPage(int page);
        int CountMedicinesOnStock();
        List<Medicine> TakeMedicinesFromStock(StockSortState sortState, int page = 1);
        Task<MemoryStream> ExportMedicinesToExcel(string path, string fileName);
    }
}
