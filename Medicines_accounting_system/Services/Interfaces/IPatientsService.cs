﻿using System;
using System.Collections.Generic;
using System.Text;
using Repository.Models;

namespace Services.Interfaces
{
    public interface IPatientsService
    {
        bool GetHasPreviousPage();
        bool GetHasNextPage();
        int CountPatients();
        int CountPatientsInClinic(int clinicId);
        bool AddPatient(Patient patient);
        void UpdatePatientStatus(int id, int? newStatusId);
        void EndPatientParticipation(int id);
        List<PatientStatus> GetPatientStatuses();
        Patient GetPatientById(int? id);
        List<Patient> GetPatientsFromClinic(int id);
        int GetCountOfNewPatientsInMonth();
        int GetCountEndedParticipationPatientsInMonth();
        void SetPageSize(int newPageSize);
        List<Patient> TakePatientsFromClinic(int clinicId, PatientsSortState sortState = PatientsSortState.NameAsc,
            int page = 1);
        void InitializeStatuses();
    }
}
