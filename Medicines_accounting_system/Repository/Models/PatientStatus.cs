﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Repository.Models
{
    public class PatientStatus
    {
        [Key]
        public int Id { get; set; }
        public string Status { get; set; }

        public List<Patient> Patients { get; set; }
    }
}