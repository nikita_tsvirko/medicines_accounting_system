﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Repository.Models
{
    public class Medicine
    {
        [Key]
        public int Id { get; set; }
        [StringLength(6)]
        public string MedicineId { get; set; }
        public string Description { get; set; }
        public string Form { get; set; }
        public string Container { get; set; }
        public string Status { get; set; }
        public DateTime ExpirationDateOfMedicine { get; set; }
        public DateTime DateOfReceiptToStock { get; set; }

        public int NumOnStock { get; set; }
        public bool AvailabilityOnStock { get; set; }

        public List<Storage> Storages { get; set; }

        public List<Visit> Visits { get; set; }
    }
}