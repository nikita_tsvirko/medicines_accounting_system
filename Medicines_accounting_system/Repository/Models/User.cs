﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Repository.Models
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Initials { get; set; }
        public bool Blocked { get; set; }
        public string BlockingReason { get; set; }
        public DateTime DatePasswordSet { get; set; }

        public int? ClinicId { get; set; }
        public Clinic Clinic { get; set; }

        public string TwoFactorProviderName { get; set; }
    }
}