﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Models
{
    public class Visit
    {
        [Key]
        public int Id { get; set; }
        public int Num { get; set; }
        public DateTime DateOfVisit { get; set; }
        public int MedicineId { get; set; }
        public Medicine Medicine { get; set; }
        public int PatientId { get; set; }
        public Patient Patient { get; set; }
    }
}