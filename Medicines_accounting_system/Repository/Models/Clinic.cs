﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Repository.Models
{
    public class Clinic
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int NumOfVisits { get; set; }

        public List<User> Researchers { get; set; }
        public List<Patient> Patients { get; set; }
        public List<Storage> Storages { get; set; }
    }
}