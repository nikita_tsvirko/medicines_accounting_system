﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Repository.Models
{
    public class Patient
    {
        [Key]
        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime Birthday { get; set; }
        public string Gender { get; set; }
        public bool Participation { get; set; }
        public DateTime FirstVisit { get; set; }
        public DateTime LastVisit { get; set; }
        public DateTime DateOfEndParticipation { get; set; }
        public int NumOfVisits { get; set; }
        public int MedicineId { get; set; }

        public int ClinicId { get; set; }
        public Clinic Clinic { get; set; }

        public int? PatientStatusId { get; set; }
        public PatientStatus PatientStatus { get; set; }

        public List<Visit> Visits { get; set; }
    }
}