﻿using System.ComponentModel.DataAnnotations;

namespace Repository.Models
{
    public class Storage
    {
        [Key]
        public int Id { get; set; }
        public int Count { get; set; }
        public int MedicineId { get; set; }
        public Medicine Medicine { get; set; }
        public int ClinicId { get; set; }
        public Clinic Clinic { get; set; }
    }
}
