﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Repository.Models;

namespace Repository
{
    public class DatabaseContext : IdentityDbContext<User>
    {
        public DbSet<Medicine> Medicines { get; set; }
        public DbSet<Storage> Storages { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Clinic> Clinics { get; set; }
        public DbSet<PatientStatus> PatientStatuses { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }
        
    }
}
