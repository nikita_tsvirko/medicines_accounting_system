﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Services.Interfaces;

namespace WebJob
{
    public class Functions
    {
        private readonly IUsersService _usersService;
        private readonly IPatientsService _patientsService;
        private readonly IClinicsService _clinicsService;
        private readonly IStockService _stockService;
        private readonly IEmailSender _emailSender;

        public Functions(IUsersService usersService,
            IPatientsService patientsService,
            IClinicsService clinicsService,
            IStockService stockService,
            IEmailSender emailSender)
        {
            _usersService = usersService;
            _patientsService = patientsService;
            _clinicsService = clinicsService;
            _stockService = stockService;
            _emailSender = emailSender;
        }

        public async Task FunctionOnATimer([TimerTrigger("0 0 0 1 */1 *", RunOnStartup = true)]
            TimerInfo timerInfo, TextWriter log)
        {
            var newPatients = _patientsService.GetCountOfNewPatientsInMonth();
            var endedParticipationPatient = _patientsService.GetCountEndedParticipationPatientsInMonth();
            string subject = "Monthly report";
            string body = "";
            body += $"<b>Number of new patients:</b> {newPatients}<br><br>";
            body += $"<b>Number of ended participation in the research patients:</b> {endedParticipationPatient}";
            body += "<br><br><br>";

            var clinics = _clinicsService.GetClinics();
            foreach (var clinic in clinics)
            {
                var issuedMedicineInMonth = _clinicsService.GetIssuedMedicinesFromClinicInMonth(clinic.Id);
                if (issuedMedicineInMonth.Count > 0)
                {
                    body += $"<b>Issued medicines from {clinic.Name} behind month: </b>";
                    body += "<br><table>";
                    body += "<tr><th>ID</th><th>Description</th><th>Form</th><th>Container</th></tr>";
                    foreach (var medicine in issuedMedicineInMonth)
                    {
                        body += $"<tr> <td>{medicine.MedicineId}</td> <td>{medicine.Description}</td> " +
                                $"<td>{medicine.Form}</td> <td>{medicine.Container}</td></tr>";
                    }
                    body += "</table>";
                }
                else
                {
                    body += $"<b>For the past month in the clinic {clinic.Name} was not issued medicines</b><br>";
                }
                body += "<br>";
            }
            body += "<br>";

            var receivedMedicines = _stockService.GetReceivedOnStockMedicinesInMonth();
            if(receivedMedicines.Count > 0)
            {
                body += "<b>Received medicines on stock behind month:</b>";
                body += "<br><table>";
                body += "<tr><th>ID</th><th>Description</th><th>Form</th><th>Container</th><th>Number</th><th>Status</th></tr>";

                foreach (var medicine in receivedMedicines)
                {
                    body += $"<tr> <td>{medicine.MedicineId}</td> <td>{medicine.Description}</td> " +
                            $"<td>{medicine.Form}</td> <td>{medicine.Container}</td>" +
                            $"<td>{medicine.NumOnStock}</td> <td>{medicine.Status}</td></tr>";
                }
                body += "</table>";
            }
            else
            {
                body += "<b>For a month, no new medicines were added to the stock</b><br>";
            }
            body += "<br><br>";

            foreach (var clinic in clinics)
            {
                var storagesFromClinic = _clinicsService.GetStoragesFromClinic(clinic.Id);
                if (storagesFromClinic.Count > 0)
                {
                    body += $"<b>Current stock of medicines in the clinic {clinic.Name}: </b>";
                    body += "<br><table>";
                    body += "<tr><th>ID</th><th>Description</th><th>Form</th><th>Container</th><th>Number</th></tr>";
                    foreach (var storage in storagesFromClinic)
                    {
                        var medicine = _clinicsService.GetMedicineById(storage.MedicineId);
                        body += $"<tr> <td>{medicine.MedicineId}</td> <td>{medicine.Description}</td> " +
                                $"<td>{medicine.Form}</td> <td>{medicine.Container}</td> <td>{storage.Count}</td></tr>";
                    }
                    body += "</table>";
                }
                else
                {
                    body += $"<b>In the clinic {clinic.Name} there are no medications</b><br>";
                }
                body += "<br>";
            }

            var users = _usersService.GetUsers();
            log.WriteLine("Emails sent to:");
            foreach (var user in users)
            {
                if (user.Email != null)
                {
                    log.WriteLine(user.Email);
                    await _emailSender.SendEmailAsync(user.Email, subject, body);
                }
            }
        }
    }
}