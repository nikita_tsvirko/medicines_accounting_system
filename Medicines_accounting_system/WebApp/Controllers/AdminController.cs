﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Repository.Models;
using Services.Interfaces;
using WebApp.Extensions;
using WebApp.Models.AdminViewModels;
using WebApp.Models.ServicesViewModels;

namespace WebApp.Controllers
{
    [Authorize(Roles = "sponsor, researcher")]
    public class AdminController : Controller
    {
        private readonly IEmailSender _emailSender;
        private readonly IUsersService _usersService;
        private readonly IClinicsService _clinicsService;
        private readonly IMapper _mapper;

        [TempData]
        public string StatusMessage { get; set; }

        public AdminController(IEmailSender emailSender, IUsersService usersService, 
            IClinicsService clinicsService, IMapper mapper)
        {
            _emailSender = emailSender;
            _usersService = usersService;
            _clinicsService = clinicsService;
            _mapper = mapper;
        }

        public IActionResult Index(int page = 1)
        {
            var model = new UsersViewModel
            {
                Users = _mapper.Map<List<UserViewModel>>(_usersService.TakeUsers(page)),
                HasPreviousPage = _usersService.GetHasPreviousPage(),
                HasNextPage = _usersService.GetHasNextPage()
            };
            ViewData["Page"] = page;
            return View(model);
        }

        public IActionResult IndexPartial(int page = 1)
        {
            var model = new UsersViewModel
            {
                Users = _mapper.Map<List<UserViewModel>>(_usersService.TakeUsers(page)),
                HasPreviousPage = _usersService.GetHasPreviousPage(),
                HasNextPage = _usersService.GetHasNextPage(),
                StatusMessage = StatusMessage
            };
            ViewData["Page"] = page;
            return PartialView(model);
        }

        [Authorize(Roles = "sponsor")]
        public async Task<IActionResult> Edit(string userId)
        {
            var user = await _usersService.GetUserByIdAsync(userId);
            var userRoles = await _usersService.GetUserRoles(user);
            var allRoles = _usersService.GetRoles();
            var clinics = _mapper.Map<List<ClinicViewModel>>(_clinicsService.GetClinics());
            var model = _mapper.Map<ChangeUserViewModel>(user);
            model.AllRoles = allRoles;
            model.UserRoles = userRoles;
            model.Clinics = clinics;

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "sponsor")]
        public async Task<IActionResult> EditPartial(ChangeUserViewModel model)
        {
            var clinics = _mapper.Map<List<ClinicViewModel>>(_clinicsService.GetClinics());
            model.AllRoles = _usersService.GetRoles();
            model.Clinics = clinics;
            if (ModelState.IsValid)
            {
                var user = await _usersService.GetUserByIdAsync(model.Id);
                var userUpdate = _mapper.Map<User>(model);
                userUpdate.Blocked = user.Blocked;
                userUpdate.BlockingReason = user.BlockingReason;
                userUpdate.DatePasswordSet = user.DatePasswordSet;
                var result = await _usersService.UpdateUserById(model.Id, userUpdate, model.UserRoles);
                if (result.Succeeded)
                {
                    model.StatusMessage = "User profile has been updated";
                    return PartialView(model);
                }
                AddErrors(result);
            }
            return PartialView(model);
        }

        [Authorize(Roles = "sponsor")]
        public async Task<IActionResult> Block(string userId)
        {
            var user = await _usersService.GetUserByIdAsync(userId);
            var model = _mapper.Map<BlockUserViewModel>(user);
            model.StatusMessage = StatusMessage;

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "sponsor")]
        public async Task<IActionResult> BlockPartial(BlockUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _usersService.BlockUser(model.Id, model.BlockingReason);
                if (result.Succeeded)
                {
                    model.Blocked = true;
                    model.StatusMessage = "User has been blocked";
                    return PartialView(model);
                }
                AddErrors(result);
            }

            return PartialView(model);
        }

        [Authorize(Roles = "sponsor")]
        public async Task<IActionResult> Unblock(string userId, int page = 1)
        {
            var result = await _usersService.UnblockUser(userId);
            if (result.Succeeded)
            {
                StatusMessage = "User has been unblocked";
                return RedirectToAction("IndexPartial", new {page});
            }
            StatusMessage = "Error: user not found";
            return RedirectToAction("IndexPartial", new { page });
        }

        [Authorize(Roles = "sponsor")]
        public async Task<IActionResult> ResetPassword(string userId)
        {
            var user = await _usersService.GetUserByIdAsync(userId);
            var code = await _usersService.GetPasswordResetTokenAsync(user);
            var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
            await _emailSender.SendEmailAsync(user.Email, "Reset Password",
                $"The research sponsor reset your password. Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
            StatusMessage = "User password has been reseted";
            return PartialView("_StatusMessage", StatusMessage);
        }

        [Authorize(Roles = "researcher")]
        public async Task<IActionResult> ViewUser(string userId)
        {
            var user = await _usersService.GetUserByIdAsync(userId);
            var userRoles = await _usersService.GetUserRoles(user);
            var model = _mapper.Map<UserViewModel>(user);
            var clinic = _mapper.Map<ClinicViewModel>(_clinicsService.GetClinicById(user.ClinicId));
            model.UserRoles = userRoles;
            model.Clinic = clinic;

            return View(model);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}