﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;
using WebApp.Models.ServicesViewModels;
using Repository.Models;
using Services.Interfaces;

namespace WebApp.Controllers
{
    public class ClinicController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IClinicsService _clinicsService;
        private readonly IUsersService _usersService;
        private readonly IPatientsService _patientsService;
        private readonly IVisitsService _visitsService;

        [TempData]
        public string StatusMessage { get; set; }

        public ClinicController(
            IMapper mapper, 
            IClinicsService clinicsService, 
            IUsersService usersService, 
            IPatientsService patientsService, 
            IVisitsService visitsService)
        {
            _mapper = mapper;
            _clinicsService = clinicsService;
            _usersService = usersService;
            _patientsService = patientsService;
            _visitsService = visitsService;
        }

        [Authorize(Roles = "sponsor")]
        public IActionResult AddClinic()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "sponsor")]
        public IActionResult AddClinic(ClinicViewModel clinic)
        {
            if (clinic != null && ModelState.IsValid)
            {
                _clinicsService.AddClinic(_mapper.Map<Clinic>(clinic));
                return Content("Clinic successfully added");
            }
            return Content("The data was entered incorrectly");
        }

        [Authorize(Roles = "researcher")]
        public IActionResult AddPatient()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "researcher")]
        public IActionResult AddPatient(PatientViewModel patient)
        {
            if (patient != null && ModelState.IsValid)
            {
                patient.ClinicId = _usersService.GetUserAsync(User).Result.ClinicId;
                if(patient.ClinicId == null)
                {
                    return Content("Error: You are not tied to any of the clinics.");
                }
                patient.Participation = true;
                patient.LastVisit = DateTime.Now;
                if (_patientsService.AddPatient(_mapper.Map<Patient>(patient)))
                {
                    return Content("Patient successfully added");
                }
                return Content("Error: It is impossible to prescribe a medicine to the patient, since the clinic stock is empty");
            }
            return Content("Error: The data was entered incorrectly");
        }

        [Authorize(Roles = "researcher")]
        public IActionResult ViewPatient(int patientId)
        {
            var patient = _patientsService.GetPatientById(patientId);

            var model = _mapper.Map<PatientViewModel>(patient);
            model.Visits = _mapper.Map<List<VisitViewModel>>(_visitsService.GetVisitsByPatient(patientId));
            model.PatientStatuses = _mapper.Map<List<PatientStatusViewModel>>(_patientsService.GetPatientStatuses());
            model.PrescribedMedicine = _mapper.Map<MedicineViewModel>(_clinicsService.GetMedicineById(patient.MedicineId));
            model.StatusMessage = StatusMessage;
            ViewBag.Participation = model.Participation ? "Yes" : "No";

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "researcher")]
        public IActionResult ViewPatient(PatientViewModel model)
        {
            if (ModelState.IsValid)
            {
                _patientsService.UpdatePatientStatus(model.Id, model.PatientStatusId);
                StatusMessage = "Patient status has been updated";
                return PartialView("_StatusMessage", StatusMessage);
            }
            StatusMessage = "Error: The data was entered incorrectly";
            return PartialView("_StatusMessage", StatusMessage);
        }

        [Authorize(Roles = "sponsor, researcher")]
        public IActionResult ClinicStatistics(int page = 1)
        {
            var model = new ClinicStatisticsViewModel
            {
                ClinicId = _usersService.GetUserAsync(User).Result.ClinicId,
                Clinics = _mapper.Map<List<ClinicViewModel>>(_clinicsService.GetClinics())
            };
            if (model.ClinicId != null)
            {
                model.CountIssuedMedicines = _clinicsService.GetCountIssuedMedicinesFromClinic(model.ClinicId);
                model.IssuedMedicines = _mapper.Map<List<MedicineViewModel>>(_clinicsService.GetIssuedMedicinesFromClinic(model.ClinicId));
                model.Clinic = _mapper.Map<ClinicViewModel>(_clinicsService.GetClinicById(model.ClinicId));
                model.Patients = _mapper.Map<List<PatientViewModel>>(_patientsService.TakePatientsFromClinic((int)model.ClinicId, PatientsSortState.NameAsc, page));
                model.HasNextPage = _patientsService.GetHasNextPage();
                model.HasPreviousPage = _patientsService.GetHasPreviousPage();
            }
            ViewData["Page"] = page;
            return View(model);
        }

        [Authorize(Roles = "sponsor, researcher")]
        public IActionResult ClinicStatisticsPartial(ClinicStatisticsViewModel model, int page = 1, PatientsSortState sortState = PatientsSortState.NameAsc)
        {
            ViewData["Page"] = page;
            ViewBag.SortState = sortState;
            ViewBag.Name = sortState == PatientsSortState.NameDesc ? PatientsSortState.NameDesc : PatientsSortState.NameAsc;
            ViewBag.Birthday = sortState == PatientsSortState.BirthdayDesc ? PatientsSortState.BirthdayDesc : PatientsSortState.BirthdayAsc;
            ViewBag.LastVisit = sortState == PatientsSortState.LastVisitDesc ? PatientsSortState.LastVisitDesc : PatientsSortState.LastVisitAsc;
            if (model.ClinicId != null)
            {
                model.CountIssuedMedicines = _clinicsService.GetCountIssuedMedicinesFromClinic(model.ClinicId);
                model.IssuedMedicines = _mapper.Map<List<MedicineViewModel>>(_clinicsService.GetIssuedMedicinesFromClinic(model.ClinicId));
                model.Clinic = _mapper.Map<ClinicViewModel>(_clinicsService.GetClinicById(model.ClinicId));
                model.Patients = _mapper.Map<List<PatientViewModel>>(_patientsService.TakePatientsFromClinic((int)model.ClinicId, sortState, page));
                model.HasNextPage = _patientsService.GetHasNextPage();
                model.HasPreviousPage = _patientsService.GetHasPreviousPage();
            }
            return PartialView(model);
        }

        [Authorize(Roles = "researcher")]
        public IActionResult AddVisit(int patientId)
        {
            var statusMessage = _visitsService.AddVisitByPatient(patientId) ? "Visit has been added" 
                : "Error: Visit hasn't been added: clinic haven't medicines";
            var patient = _patientsService.GetPatientById(patientId);
            var model = _mapper.Map<PatientViewModel>(patient);
            model.PrescribedMedicine = _mapper.Map<MedicineViewModel>(_clinicsService.GetMedicineById(patient.MedicineId));
            model.Visits = _mapper.Map<List<VisitViewModel>>(_visitsService.GetVisitsByPatient(patientId));
            model.PatientStatuses = _mapper.Map<List<PatientStatusViewModel>>(_patientsService.GetPatientStatuses());
            model.StatusMessage = statusMessage;
            ViewBag.Participation = model.Participation ? "Yes" : "No";
            return PartialView("ViewPatientPartial", model);
        }

        [Authorize(Roles = "researcher")]
        public IActionResult EndingParticipation(int patientId)
        {
            _patientsService.EndPatientParticipation(patientId);
            var patient = _patientsService.GetPatientById(patientId);
            var model = _mapper.Map<PatientViewModel>(patient);
            model.PrescribedMedicine = _mapper.Map<MedicineViewModel>(_clinicsService.GetMedicineById(patient.MedicineId));
            model.Visits = _mapper.Map<List<VisitViewModel>>(_visitsService.GetVisitsByPatient(patientId));
            model.PatientStatuses = _mapper.Map<List<PatientStatusViewModel>>(_patientsService.GetPatientStatuses());
            model.StatusMessage = "Patient participation in research has been ended";
            ViewBag.Participation = model.Participation ? "Yes" : "No";
            return PartialView("ViewPatientPartial", model);
        }

        public IActionResult PatientsSort(int clinicId, PatientsSortState sortState = PatientsSortState.NameAsc)
        {
            PatientsSortState newSortState;
            if (sortState == PatientsSortState.NameAsc || sortState == PatientsSortState.NameDesc)
            {
                newSortState = sortState == PatientsSortState.NameAsc ? PatientsSortState.NameDesc : PatientsSortState.NameAsc;
            }
            else
            if (sortState == PatientsSortState.BirthdayAsc || sortState == PatientsSortState.BirthdayDesc)
            {
                newSortState = sortState == PatientsSortState.BirthdayAsc ? PatientsSortState.BirthdayDesc : PatientsSortState.BirthdayAsc;
            }
            else
            {
                newSortState = sortState == PatientsSortState.LastVisitAsc ? PatientsSortState.LastVisitDesc : PatientsSortState.LastVisitAsc;
            }
            return RedirectToAction("ClinicStatisticsPartial", new { clinicId, sortState = newSortState });
        }
    }
}
