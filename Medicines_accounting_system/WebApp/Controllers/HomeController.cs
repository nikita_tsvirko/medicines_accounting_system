﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models.ServicesViewModels;
using Services.Interfaces;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPatientsService _patientsService;
        private readonly IVisitsService _visitsService;

        public HomeController(IPatientsService patientsService, 
            IVisitsService visitsService)
        {
            _patientsService = patientsService;
            _visitsService = visitsService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "sponsor, researcher")]
        public IActionResult ResearchStatistics()
        {
            var model = new ResearchStatisticsViewModel
            {
                CountPatients = _patientsService.CountPatients(),
                CountVisits = _visitsService.CountVisits(),
                NumOfVisitsForPatients = _visitsService.GetNumOfVisitsForPatients()
            };
            return View(model);
        }
    }
}
