﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Repository.Models;
using Services.Interfaces;
using WebApp.Extensions;
using WebApp.Models.ManageViewModels;
using WebApp.Models.ServicesViewModels;

namespace WebApp.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class ManageController : Controller
    {
        private readonly IEmailSender _emailSender;
        private readonly IUsersService _usersService;
        private readonly IClinicsService _clinicsService;
        private readonly IMapper _mapper;
        private readonly UrlEncoder _urlEncoder;

        private const string AuthenticatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";
        private const string RecoveryCodesKey = nameof(RecoveryCodesKey);

        public ManageController(
          IEmailSender emailSender,
          IUsersService usersService,
          IClinicsService clinicsService,
          IMapper mapper,
          UrlEncoder urlEncoder)
        {
            _emailSender = emailSender;
            _clinicsService = clinicsService;
            _usersService = usersService;
            _mapper = mapper;
            _urlEncoder = urlEncoder;
        }

        [TempData]
        public string StatusMessage { get; set; }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var user = await _usersService.GetUserAsync(User);
            var model = _mapper.Map<IndexViewModel>(user);
            model.Clinic = _mapper.Map<ClinicViewModel>(_clinicsService.GetClinicById(user.ClinicId));
            model.StatusMessage = StatusMessage;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(IndexViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _usersService.GetUserByIdAsync(model.Id);
                var userUpdate = _mapper.Map<User>(model);
                userUpdate.ClinicId = user.ClinicId;
                userUpdate.Blocked = user.Blocked;
                userUpdate.BlockingReason = user.BlockingReason;
                userUpdate.DatePasswordSet = user.DatePasswordSet;
                var result = await _usersService.UpdateCurrentUser(User, userUpdate);
                if (result.Succeeded)
                {
                    StatusMessage = "Your profile has been updated";
                    return RedirectToAction(nameof(Index));
                }
                AddErrors(result);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendVerificationEmail(IndexViewModel model)
        {
            var user = await _usersService.GetUserAsync(User);
            var code = await _usersService.GetEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
            await _emailSender.SendEmailAsync(model.Email, "Confirm your account",
                $"Confirm registration by clicking on the link: <a href='{callbackUrl}'>link</a>");

            StatusMessage = "Verification email sent. Please check your email.";
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> ChangePassword()
        {
            var user = await _usersService.GetUserAsync(User);
            var model = new ChangePasswordViewModel { Id = user.Id, StatusMessage = StatusMessage };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            string statusMessage;
            if (ModelState.IsValid)
            {
                if (model.NewPassword == model.OldPassword)
                {
                    statusMessage = "Error: This password has already been used.";
                    return PartialView("_StatusMessage", statusMessage);
                }
                var changePasswordResult = await _usersService.ChangePassword(User, model.OldPassword, model.NewPassword);
                if (changePasswordResult.Succeeded)
                {
                    statusMessage = "Your password has been changed.";
                    return PartialView("_StatusMessage", statusMessage);
                }
                AddErrors(changePasswordResult);
            }
            statusMessage = "Error: The data was entered incorrectly:<br>";
            var allErrors = ModelState.Values.SelectMany(v => v.Errors.Select(b => b.ErrorMessage));
            foreach (var error in allErrors)
            {
                statusMessage += $"{error}<br>";
            }
            return PartialView("_StatusMessage", statusMessage);
        }

        public async Task<IActionResult> ResetPassword(string userId)
        {
            var user = await _usersService.GetUserByIdAsync(userId);
            var code = await _usersService.GetPasswordResetTokenAsync(user);
            var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
            await _emailSender.SendEmailAsync(user.Email, "Reset Password",
                $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
            var statusMessage = "User password has been reseted";
            return PartialView("_StatusMessage", statusMessage);
        }

        [HttpGet]
        public async Task<IActionResult> TwoFactorAuthentication()
        {
            var user = await _usersService.GetUserAsync(User);
            var model = new TwoFactorAuthenticationViewModel
            {
                HasAuthenticator = await _usersService.GetAuthenticatorKeyAsync(user) != null,
                Is2faEnabled = user.TwoFactorEnabled,
                RecoveryCodesLeft = await _usersService.CountRecoveryCodesAsync(user),
                ProviderName = user.TwoFactorProviderName,
                StatusMessage = StatusMessage
            };

            return View(model);
        }
        public async Task<IActionResult> Disable2fa()
        {
            var user = await _usersService.GetUserAsync(User);
            await _usersService.SetTwoFactorEnabledAsync(user, false, null);
            StatusMessage = "Two-Factor authentication disabled";
            return RedirectToAction(nameof(TwoFactorAuthentication));
        }

        [HttpGet]
        public async Task<IActionResult> EnableEmail2fa()
        {
            var user = await _usersService.GetUserAsync(User);
            await _usersService.SetTwoFactorEnabledAsync(user, true, TokenProviderNames.EmailTwoFactor.ToString());
            StatusMessage = "Two-Factor authentication with Email successfully activated";
            return RedirectToAction(nameof(TwoFactorAuthentication));
        }

        [HttpGet]
        public async Task<IActionResult> EnableAuthenticator()
        {
            var user = await _usersService.GetUserAsync(User);

            var model = new EnableAuthenticatorViewModel();
            await LoadSharedKeyAndQrCodeUriAsync(user, model);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EnableAuthenticator(EnableAuthenticatorViewModel model)
        {
            var user = await _usersService.GetUserAsync(User);

            if (!ModelState.IsValid)
            {
                await LoadSharedKeyAndQrCodeUriAsync(user, model);
                return View(model);
            }

            var verificationCode = model.Code.Replace(" ", string.Empty).Replace("-", string.Empty);

            var is2faTokenValid = await _usersService.VerifyTwoFactorTokenAsync(
                user, verificationCode);

            if (!is2faTokenValid)
            {
                ModelState.AddModelError("Code", "Verification code is invalid.");
                await LoadSharedKeyAndQrCodeUriAsync(user, model);
                return View(model);
            }

            await _usersService.SetTwoFactorEnabledAsync(user, true, TokenOptions.DefaultAuthenticatorProvider);
            var recoveryCodes = await _usersService.GenerateNewTwoFactorRecoveryCodesAsync(user);
            TempData[RecoveryCodesKey] = recoveryCodes.ToArray();

            return RedirectToAction(nameof(ShowRecoveryCodes));
        }

        [HttpGet]
        public IActionResult ShowRecoveryCodes()
        {
            var recoveryCodes = (string[])TempData[RecoveryCodesKey];
            if (recoveryCodes == null)
            {
                return RedirectToAction(nameof(TwoFactorAuthentication));
            }
            return View(recoveryCodes);
        }

        [HttpGet]
        public IActionResult ResetAuthenticatorWarning()
        {
            return View(nameof(ResetAuthenticator));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetAuthenticator()
        {
            var user = await _usersService.GetUserAsync(User);

            await _usersService.SetTwoFactorEnabledAsync(user, false, null);
            await _usersService.ResetAuthenticatorKeyAsync(user);
            return RedirectToAction(nameof(EnableAuthenticator));
        }

        [HttpGet]
        public async Task<IActionResult> GenerateRecoveryCodesWarning()
        {
            var user = await _usersService.GetUserAsync(User);

            if (!user.TwoFactorEnabled)
            {
                throw new ApplicationException($"Cannot generate recovery codes for user with ID '{user.Id}' because they do not have 2FA enabled.");
            }

            return View(nameof(GenerateRecoveryCodes));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GenerateRecoveryCodes()
        {
            var user = await _usersService.GetUserAsync(User);

            if (!user.TwoFactorEnabled)
            {
                throw new ApplicationException($"Cannot generate recovery codes for user with ID '{user.Id}' as they do not have 2FA enabled.");
            }

            var recoveryCodes = await _usersService.GenerateNewTwoFactorRecoveryCodesAsync(user);

            return View(nameof(ShowRecoveryCodes), recoveryCodes);
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private string FormatKey(string unformattedKey)
        {
            var result = new StringBuilder();
            int currentPosition = 0;
            while (currentPosition + 4 < unformattedKey.Length)
            {
                result.Append(unformattedKey.Substring(currentPosition, 4)).Append(" ");
                currentPosition += 4;
            }
            if (currentPosition < unformattedKey.Length)
            {
                result.Append(unformattedKey.Substring(currentPosition));
            }

            return result.ToString().ToLowerInvariant();
        }

        private async Task LoadSharedKeyAndQrCodeUriAsync(User user, EnableAuthenticatorViewModel model)
        {
            var unformattedKey = await _usersService.GetAuthenticatorKeyAsync(user);
            if (string.IsNullOrEmpty(unformattedKey))
            {
                await _usersService.ResetAuthenticatorKeyAsync(user);
                unformattedKey = await _usersService.GetAuthenticatorKeyAsync(user);
            }

            model.SharedKey = FormatKey(unformattedKey);
            model.AuthenticatorUri = string.Format(AuthenticatorUriFormat,
                _urlEncoder.Encode("Medicines accounting system"),
                _urlEncoder.Encode(user.Email),
                unformattedKey);
        }

        #endregion
    }
}
