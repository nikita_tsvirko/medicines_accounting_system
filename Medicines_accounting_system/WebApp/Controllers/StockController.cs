﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using WebApp.Models.ServicesViewModels;
using Repository.Models;
using Services.Interfaces;

namespace WebApp.Controllers
{
    public class StockController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IClinicsService _clinicsService;
        private readonly IStockService _stockService;
        private readonly IHostingEnvironment _hostingEnvironment;

        [TempData]
        public string StatusMessage { get; set; }

        public StockController(IMapper mapper, 
            IClinicsService clinicsService, 
            IStockService stockService,
            IHostingEnvironment hostingEnvironment)
        {
            _mapper = mapper;
            _clinicsService = clinicsService;
            _stockService = stockService;
            _hostingEnvironment = hostingEnvironment;
        }
        
        [Authorize(Roles = "manager")]
        public IActionResult AddMedicineToStock()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "manager")]
        public IActionResult AddMedicineToStock(MedicineViewModel medicine)
        {
            if (medicine != null && ModelState.IsValid)
            {
                _stockService.AddMedicine(_mapper.Map<Medicine>(medicine));
                return Content("Medicine successfully added to stock");
            }
            return Content("The data was entered incorrectly");
        }

        [Authorize(Roles = "manager")]
        public IActionResult EditMedicineOnStock(int medicineId)
        {
            var medicine = _stockService.GetMedicineFromStock(medicineId);

            var model = _mapper.Map<MedicineViewModel>(medicine);
            model.StatusMessage = StatusMessage;

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "manager")]
        public IActionResult EditMedicineOnStock(MedicineViewModel model)
        {
            if (ModelState.IsValid)
            {
                var medicineUpdate = _mapper.Map<Medicine>(model);
                _stockService.UpdateMedicineOnStock(model.Id, medicineUpdate);
                StatusMessage = "Medicine data has been updated";
                return PartialView("_StatusMessage", StatusMessage);
            }
            StatusMessage = "Error: The data was entered incorrectly:<br>";
            var allErrors = ModelState.Values.SelectMany(v => v.Errors.Select(b => b.ErrorMessage));
            foreach (var error in allErrors)
            {
                StatusMessage += $"{error}<br>";
            }
            return PartialView("_StatusMessage", StatusMessage);
        }

        [Authorize(Roles = "manager")]
        public IActionResult SendMedicineToClinic()
        {
            var model = new SentMedicineView
            {
                Clinics = _mapper.Map<List<ClinicViewModel>>(_clinicsService.GetClinics()),
                Medicines = _mapper.Map<List<MedicineViewModel>>(_stockService.GetMedicinesFromStock()),
                StatusMessage = StatusMessage
            };
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "manager")]
        public IActionResult SendMedicineToClinic(SentMedicineView model)
        {
            if (model != null && ModelState.IsValid)
            {
                var sentResult = _stockService.SendMedicine(model.MedicineId, model.Count);
                if (sentResult)
                {
                    _clinicsService.AddMedicineToClinic(model.ClinicId, model.MedicineId, model.Count);
                    StatusMessage = "Medicines were successfully sent out";
                    return PartialView("_StatusMessage", StatusMessage);
                }
                StatusMessage = "Error: Not enough medicine in the stock";
                return PartialView("_StatusMessage", StatusMessage);
            }
            StatusMessage = "Error: The form was entered incorrectly";
            return PartialView("_StatusMessage", StatusMessage);
        }

        [Authorize(Roles = "manager")]
        public IActionResult AutoSendMedicines()
        {
            var sentResult = _stockService.AutomaticSendMedicinesToClinic();
            if (sentResult != null)
            {
                var totalCount = sentResult.Sum(s => s.Count);
                if (totalCount == 0)
                {
                    StatusMessage = "Error: In the clinics a sufficient supply of medicines";
                    return PartialView("_StatusMessage", StatusMessage);
                }
                StatusMessage = "Medicines were successfully sent out:<br/>";
                foreach (var storage in sentResult)
                {
                    if(storage.Count > 0)
                    {
                        var clinic = _clinicsService.GetClinicById(storage.ClinicId);
                        StatusMessage += $"Medicine with ID {storage.Medicine.MedicineId} in amount {storage.Count} to the {clinic.Name} clinic<br/>";
                    }
                }
                return PartialView("_StatusMessage", StatusMessage);
            }
            StatusMessage = "Error: Not enough medicines in the stock";
            return PartialView("_StatusMessage", StatusMessage);
        }

        [Authorize(Roles = "manager")]
        public ActionResult SendMedicinesFromExcelTable()
        {
            IFormFile file = Request.Form.Files[0];
            string folderName = "Upload";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName);
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            if (_stockService.SendMedicinesFromExcelTable(file, newPath))
            {
                StatusMessage = "Medicines were successfully sent out";
                return RedirectToAction("SendMedicineToClinic");
            }
            StatusMessage = "Error: Error adding medicines";
            return RedirectToAction("SendMedicineToClinic");
        }

        [Authorize(Roles = "sponsor, manager")]
        public IActionResult StockManagement(int page = 1)
        {
            ViewData["Page"] = page;
            ViewBag.SortState = StockSortState.IdAsc;
            return View(_stockService);
        }

        [Authorize(Roles = "sponsor, manager")]
        public IActionResult StockManagementPartial(int page = 1, StockSortState sortState = StockSortState.IdAsc)
        {
            ViewData["Page"] = page;
            ViewBag.SortState = sortState;
            ViewBag.Id = sortState == StockSortState.IdDesc ? StockSortState.IdDesc : StockSortState.IdAsc;
            ViewBag.Form = sortState == StockSortState.FormDesc ? StockSortState.FormDesc : StockSortState.FormAsc;
            ViewBag.Container = sortState == StockSortState.ContainerDesc ? StockSortState.ContainerDesc : StockSortState.ContainerAsc;
            ViewBag.Number = sortState == StockSortState.NumOfStockDesc ? StockSortState.NumOfStockDesc : StockSortState.NumOfStockAsc;
            ViewBag.ExpirationDate = sortState == StockSortState.ExpirationDateOfMedicineDesc ? StockSortState.ExpirationDateOfMedicineDesc : StockSortState.ExpirationDateOfMedicineAsc;
            return PartialView(_stockService);
        }

        [Authorize(Roles = "manager")]
        public IActionResult DeleteMedicine(int medicineId)
        {
            _stockService.DeleteMedicineFromStock(medicineId);
            return RedirectToAction("StockManagementPartial");
        }
        
        public IActionResult StockSort(StockSortState sortState = StockSortState.IdAsc)
        {
            StockSortState newSortState;
            if (sortState == StockSortState.IdAsc || sortState == StockSortState.IdDesc)
            {
                newSortState = sortState == StockSortState.IdAsc ? StockSortState.IdDesc : StockSortState.IdAsc;
            }
            else
            if (sortState == StockSortState.FormAsc || sortState == StockSortState.FormDesc)
            {
                newSortState = sortState == StockSortState.FormAsc ? StockSortState.FormDesc : StockSortState.FormAsc;
            }
            else
            if (sortState == StockSortState.ContainerAsc || sortState == StockSortState.ContainerDesc)
            {
                newSortState = sortState == StockSortState.ContainerAsc ? StockSortState.ContainerDesc : StockSortState.ContainerAsc;
            }
            else
            if (sortState == StockSortState.NumOfStockAsc || sortState == StockSortState.NumOfStockDesc)
            {
                newSortState = sortState == StockSortState.NumOfStockAsc ? StockSortState.NumOfStockDesc : StockSortState.NumOfStockAsc;
            }
            else
            {
                newSortState = sortState == StockSortState.ExpirationDateOfMedicineAsc ? StockSortState.ExpirationDateOfMedicineDesc : StockSortState.ExpirationDateOfMedicineAsc;
            }
            return RedirectToAction("StockManagementPartial", new { sortState = newSortState });
        }

        public async Task<IActionResult> ExportStockToExcel()
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = $@"stock_{DateTime.Now:dd_MM_yyyy}.xlsx";
            var memory = await _stockService.ExportMedicinesToExcel(sWebRootFolder, sFileName);
            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
        }
    }
}
