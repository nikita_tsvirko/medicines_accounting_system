﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.ServicesViewModels
{
    public class ResearchStatisticsViewModel
    {
        public int CountPatients { get; set; }
        public int CountVisits { get; set; }
        public int NumOfVisitsForPatients { get; set; }
    }
}