﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.ServicesViewModels
{
    public enum GenderOfPatient
    {
        Unknown = 0,
        Male = 1,
        Female = 2
    }
}