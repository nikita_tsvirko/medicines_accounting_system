﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.ServicesViewModels
{
    public class VisitViewModel
    {
        public int Num { get; set; }
        public DateTime DateOfVisit { get; set; }
    }
}
