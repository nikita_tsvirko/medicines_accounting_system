﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.ServicesViewModels
{
    public class PatientStatusViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Status { get; set; }
    }
}
