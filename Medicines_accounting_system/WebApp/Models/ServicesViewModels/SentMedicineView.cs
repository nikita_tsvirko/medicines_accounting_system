﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Repository.Models;

namespace WebApp.Models.ServicesViewModels
{
    public class SentMedicineView
    {
        [Required]
        public int ClinicId { get; set; }
        [Required]
        public int MedicineId { get; set; }
        [Required]
        [Range(1, 999999, ErrorMessage = "Count entered incorrectly")]
        public int Count { get; set; }
        public List<ClinicViewModel> Clinics { get; set; }
        public List<MedicineViewModel> Medicines{ get; set; }

        public string StatusMessage { get; set; }

        public SentMedicineView()
        {
            Clinics = new List<ClinicViewModel>();
            Medicines = new List<MedicineViewModel>();
        }
    }
}
