﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.ServicesViewModels
{
    public class ClinicStatisticsViewModel
    {
        public int CountIssuedMedicines { get; set; }
        public int? ClinicId { get; set; }
        public ClinicViewModel Clinic { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
        public List<PatientViewModel> Patients { get; set; }
        public List<ClinicViewModel> Clinics { get; set; }
        public List<MedicineViewModel> IssuedMedicines { get; set; }

        public ClinicStatisticsViewModel()
        {
            Patients = new List<PatientViewModel>();
            Clinics = new List<ClinicViewModel>();
            IssuedMedicines = new List<MedicineViewModel>();
        }
    }
}
