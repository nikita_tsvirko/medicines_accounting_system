﻿namespace WebApp.Models.ServicesViewModels
{
    public enum TokenProviderNames
    {
        Authenticator,
        EmailTwoFactor
    }
}