﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Repository.Models;

namespace WebApp.Models.ServicesViewModels
{
    public class PatientViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Full name")]
        public string FullName { get; set; }
        [Required]
        public DateTime Birthday { get; set; }
        [Required]
        public GenderOfPatient Gender { get; set; }
        public int? ClinicId { get; set; }
        public int? PatientStatusId { get; set; }
        public bool Participation { get; set; }
        public DateTime LastVisit { get; set; }
        public MedicineViewModel PrescribedMedicine { get; set; }

        public List<VisitViewModel> Visits { get; set; }
        public List<PatientStatusViewModel> PatientStatuses { get; set; }

        public string StatusMessage { get; set; }

        public PatientViewModel()
        {
            Visits = new List<VisitViewModel>();
            PatientStatuses = new List<PatientStatusViewModel>();
        }
    }
}
