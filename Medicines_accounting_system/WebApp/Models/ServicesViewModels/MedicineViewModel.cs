﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.ServicesViewModels
{
    public class MedicineViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "ID")]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "ID should consist of 6 digits")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "ID should consist of only digits")]
        public string MedicineId { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "Field Form too long")]
        public string Form { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "Field Container too long")]
        public string Container { get; set; }

        public StatusOfMedicine Status { get; set; }
        [Display(Name = "Expiration date")]
        public DateTime ExpirationDateOfMedicine { get; set; }
        [Required]
        [Display(Name = "Number")]
        [Range(0, 999999, ErrorMessage = "Number entered incorrectly")]
        public int NumOnStock { get; set; }
        public bool AvailabilityOnStock { get; set; }

        public string StatusMessage { get; set; }
    }
}
