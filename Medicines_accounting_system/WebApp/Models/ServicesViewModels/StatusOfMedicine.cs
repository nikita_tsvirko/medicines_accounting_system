﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.ServicesViewModels
{
    public enum StatusOfMedicine
    {
        Unknown = 0,
        Damaged = 1,
        Intact = 2,
        Lost = 3,
        Expired = 4
    }
}