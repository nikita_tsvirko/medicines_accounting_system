﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.ServicesViewModels;

namespace WebApp.Models.ManageViewModels
{
    public class IndexViewModel
    {
        public string Id { get; set; }
        public string Username { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [RegularExpression(@"\D+", ErrorMessage = "First Name must not contain digits.")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [RegularExpression(@"\D+", ErrorMessage = "Last Name must not contain digits.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Initials")]
        [RegularExpression(@"[A-Za-zА-Яа-я]*", ErrorMessage = "Initials should consist of only symbols.")]
        public string Initials { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public ClinicViewModel Clinic { get; set; }

        public string StatusMessage { get; set; }
    }
}
