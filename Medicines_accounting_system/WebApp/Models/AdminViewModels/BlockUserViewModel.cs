﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.AdminViewModels
{
    public class BlockUserViewModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public bool Blocked { get; set; }

        [Required]
        [Display(Name = "Blocking reason")]
        public string BlockingReason { get; set; }

        public string StatusMessage { get; set; }
    }
}
