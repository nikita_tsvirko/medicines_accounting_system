﻿using System.Collections.Generic;

namespace WebApp.Models.AdminViewModels
{
    public class UsersViewModel
    {
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
        public List<UserViewModel> Users { get; set; }

        public string StatusMessage { get; set; }

        public UsersViewModel()
        {
            Users = new List<UserViewModel>();
        }
    }
}