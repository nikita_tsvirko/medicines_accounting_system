﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.ServicesViewModels;

namespace WebApp.Models.AdminViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Initials { get; set; }
        public string Email { get; set; }
        public bool Blocked { get; set; }
        public string BlockingReason { get; set; }
        public ClinicViewModel Clinic { get; set; }
        public List<string> UserRoles { get; set; }
    }
}
