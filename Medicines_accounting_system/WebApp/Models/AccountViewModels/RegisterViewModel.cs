﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.ServicesViewModels;

namespace WebApp.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "First Name")]
        [RegularExpression(@"\D+", ErrorMessage = "First Name must not contain digits.")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [RegularExpression(@"\D+", ErrorMessage = "Last Name must not contain digits.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Initials")]
        [RegularExpression(@"[A-Za-zА-Яа-я]*", ErrorMessage = "Initials should consist of only letters.")]
        public string Initials { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public int? ClinicId { get; set; }
        public List<ClinicViewModel> Clinics { get; set; }

        public RegisterViewModel()
        {
            Clinics = new List<ClinicViewModel>();
        }

    }
}
