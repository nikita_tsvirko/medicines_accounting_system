﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository;
using Repository.Models;
using Services.Interfaces;
using Services.Realizations;

namespace WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Connection")));

            services.AddIdentity<User, IdentityRole>(options =>
                {
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromDays(30);
                    options.Password.RequireNonAlphanumeric = false;
                    options.Tokens.ProviderMap.Add("EmailTwoFactor", new TokenProviderDescriptor(typeof(IUserTwoFactorTokenProvider<User>)));
                })
                .AddEntityFrameworkStores<DatabaseContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IClinicsService, ClinicsService>();
            services.AddTransient<IPatientsService, PatientsService>();
            services.AddTransient<IVisitsService, VisitsService>();
            services.AddTransient<IStockService, StockService>();
            services.AddTransient<IUsersService, UsersService>();
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IUserTwoFactorTokenProvider<User>, DataProtectorTokenProvider<User>>();

            services.AddAutoMapper();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, 
            IUsersService usersService, IPatientsService patientsService)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();
            usersService.InitializeRoles();
            patientsService.InitializeStatuses();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            
        }
    }
}
