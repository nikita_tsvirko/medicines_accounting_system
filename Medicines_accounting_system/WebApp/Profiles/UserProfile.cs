﻿using AutoMapper;
using Repository.Models;
using WebApp.Models.AccountViewModels;
using WebApp.Models.AdminViewModels;
using WebApp.Models.ManageViewModels;

namespace WebApp.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<RegisterViewModel, User>()
                .ForMember(x => x.UserName, x => x.MapFrom(u => u.Email));

            CreateMap<User, IndexViewModel>()
                .ForMember(x => x.IsEmailConfirmed, x => x.MapFrom(u => u.EmailConfirmed));
            CreateMap<IndexViewModel, User>()
                .ForMember(x => x.EmailConfirmed, x => x.MapFrom(u => u.IsEmailConfirmed));

            CreateMap<User, ChangeUserViewModel>();
            CreateMap<ChangeUserViewModel, User>();

            CreateMap<User, BlockUserViewModel>();
            CreateMap<BlockUserViewModel, User>();

            CreateMap<User, UserViewModel>();
            CreateMap<UserViewModel, User>();
        }
    }
}