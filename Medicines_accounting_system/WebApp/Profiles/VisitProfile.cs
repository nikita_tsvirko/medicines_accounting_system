﻿using AutoMapper;
using Repository.Models;
using WebApp.Models.ServicesViewModels;

namespace WebApp.Profiles
{
    public class VisitProfile : Profile
    {
        public VisitProfile()
        {
            CreateMap<Visit, VisitViewModel>();
            CreateMap<VisitViewModel, Visit>();
        }
    }
}