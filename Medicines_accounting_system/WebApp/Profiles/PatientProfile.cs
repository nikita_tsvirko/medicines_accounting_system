﻿using AutoMapper;
using Repository.Models;
using WebApp.Models.ServicesViewModels;

namespace WebApp.Profiles
{
    public class PatientProfile : Profile
    {
        public PatientProfile()
        {
            CreateMap<Patient, PatientViewModel>();
            CreateMap<PatientViewModel, Patient>();
        }
    }
}