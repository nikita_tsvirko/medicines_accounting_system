﻿using AutoMapper;
using Repository.Models;
using WebApp.Models.ServicesViewModels;

namespace WebApp.Profiles
{
    public class MedicineProfile : Profile
    {
        public MedicineProfile()
        {
            CreateMap<Medicine, MedicineViewModel>();
            CreateMap<MedicineViewModel, Medicine>();
        }
    }
}