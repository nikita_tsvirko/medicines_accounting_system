﻿using AutoMapper;
using Repository.Models;
using WebApp.Models.ServicesViewModels;

namespace WebApp.Profiles
{
    public class PatientStatusProfile : Profile
    {
        public PatientStatusProfile()
        {
            CreateMap<PatientStatus, PatientStatusViewModel>();
            CreateMap<PatientStatusViewModel, PatientStatus>();
        }
    }
}