﻿using AutoMapper;
using Repository.Models;
using WebApp.Models.ServicesViewModels;

namespace WebApp.Profiles
{
    public class ClinicProfile : Profile
    {
        public ClinicProfile()
        {
            CreateMap<Clinic, ClinicViewModel>();
            CreateMap<ClinicViewModel, Clinic>();
        }
    }
}